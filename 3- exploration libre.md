---
layout: post
title: Exploration libre
description: Les sujets d'exploration libre
image: assets/images/pic11.jpg
nav-menu: true
show_tile: true
---

💡 La démarche d’exploration libre est une démarche moins convergée, moins managée, qui repose sur l’initiative individuelle de chacun.

Les attentes en terme de ROI sont moins fortes et moins immédiates.  
En regard, les investissements consentis par l’entreprise sont moindres (temps consacré, conférences, etc.)

Par nature, les sujets d'exploration libre ne sont pas tous identifiés, mais on peut en lister quelques uns :
* IoT / Edge computing
* Blockchain
* Ordinateur Quantique
* Nouvelles UI / VR / AR
* Programmation fonctionnelle
