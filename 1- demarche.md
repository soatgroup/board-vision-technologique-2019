---
layout: page
title: La démarche
image: assets/images/brainstorming.jpg
nav-menu: true
show_tile: true
---

<!-- Main -->
<div id="main" class="alt">

<!-- One -->
<section id="one">
	<div class="inner">
		<header class="major">
			<h1>La démarche</h1>
		</header>

<!-- Content -->
<h2 id="content">Comment a été élaborée la vision technologique pour 2019 ?</h2>
<p>La vision technologique chez <b>SOAT</b> est la boussole qui donne le cap à suivre.<br>C'est elle qui oriente les efforts que les forces vives de l'entreprise vont devoir faire et surtout où ils doivent porter.</p>
<p>💡 Les membres du Board ont souhaité fabriquer une vision qui <b>inclut</b> plus qu’elle <b>n’exclut</b>.<br>
Pour cela, ils ont construit une vision à <b>3 étages</b>…<br>
Ces 3 étages présentent des attentes différentes et donc des investissements différents, en terme de :</p>
<li> R&D
<li> Recrutement
<li> Formation (interne et externe)
<li> Capitalisation et effort marketing
<li> Construction de l'offre commerciale

<hr class="major" />

<div class="row">
	<div class="4u 12u$(medium)">
		<h3>1- les axes technologiques piliers</h3>
		<p>Il s'agit des pratiques et des domaines technologiques pour lesquels il existe un <b>marché identifié</b> déjà important et sur lesquels SOAT doit se positionner ou fournir un effort appuyé pour <b>se renforcer</b> en compétences, en visibilité en tant qu’acteur du marché, en effectifs.</p>
	</div>
	<div class="4u 12u$(medium)">
		<h3>2- les sujets d'exploration libre</h3>
		<p>Il s'agit des domaines technologiques pour lesquels il existe des <b>marchés émergents ou de niche</b> et pour lesquels certains SOATiens présentent une compétence ou une forte affinité.<br>Pour le Board, il convient de ne pas censurer ces collaborateurs dans leur volonté d’explorer ces sujets. Toutefois, comme les retours attendus sont limités, les moyens consentis sont moindres. Logique !</p>
	</div>
	<div class="4u 12u$(medium)">
		<h3>3- les sujets de montée en compétence</h3>
		<p>Il s'agit des domaines techniques sur lesquels les SOATiens doivent être et surtout <b>rester pertinents</b>, mais qui ne constituent pas une offre en tant que telle.<br>Sur ces sujets, les efforts de recrutement et de formation restent importants, les efforts de construction d’offre et de capitalisation sont moindres.</p>
	</div>
</div>