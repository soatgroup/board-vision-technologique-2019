---
layout: page
title: Les axes technologiques piliers
image: assets/images/processor.jpg
nav-menu: true
show_tile: true
---

<!-- Main -->
<div id="main" class="alt">

<!-- One -->
<section id="one">
	<div class="inner">
		<header class="major">
			<h1>Les axes technologiques piliers</h1>
		</header>

<!-- Content -->
<h2 id="content">De quoi s'agit-il ?</h2>
<p>Il s'agit des pratiques et des domaines technologiques pour lesquels il existe un <b>marché identifié</b> déjà important et sur lesquels SOAT doit se positionner ou fournir un effort appuyé pour <b>se renforcer</b> en compétences, en visibilité en tant qu’acteur du marché, en effectifs.</p>
<p>Nous les avons volontairement limités à 4 pratiques et 4 domaines technologiques.</p>
<hr class="major" />

<div class="row">
	<div class="11u 12u$(medium)">
		<h3>Pratiques</h3>
		<p><u>Exemples :</u> Agilité, Craftsmanship, DevOps, DevSecOps</p>
	</div>
	<div class="3u 12u$(medium)">
		<h3>Cloud / Dev*Ops</h3>
		<p><u><b>Principaux outils :</b></u> AWS, Azure, GCP,  IBM, AliCloud, Oracle Cloud<br>
		<u><b>Principales activités :</b></u> design, infra as code, migration vers le Cloud, automatisation, optimisation de la gouvernance économique, transition multi-cloud, sécurisation, coaching, métrologie, etc.<br>
        <u><b>Exemples :</b></u> containers, serverless, infra as code…</p>
	</div>
	<div class="3u 12u$(medium)">
		<h3>Architecture Applicative</h3>
		<p><u><b>Principaux outils :</b></u> Clean Code, Architecture, DDD, TDD<br>
		<u><b>Principales activités :</b></u> TBD<br>
        <u>Exemples :</u> CQRS, reactive architecture, microservices…</p>
	</div>
	<div class="3u 12u$(medium)">
		<h3>Intelligence artificielle</h3>
		<p><u><b>Principaux outils :</b></u> Cognitive services, machine learning, deep learning, data science<br>
        <u><b>Exemples :</b></u> Microsoft - GCP - IBM Watson  - TensorFlow</p>
	</div>
	<div class="3u 12u$(medium)">
		<h3>Développement Front</h3>
		<p><u><b>Principaux outils :</b></u> Web, mobile, UX/UI<br>
        <u><b>Exemples :</b></u> PWA, WebAsm, GraphQL, Kotlin, Swift, React Native, Flutter</p>
	</div>
</div>