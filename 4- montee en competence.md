---
layout: page
title: Les sujets de montée en compétence
image: assets/images/teachingclass.jpg
nav-menu: true
show_tile: true
---

<!-- Main -->
<div id="main" class="alt">

<!-- One -->
<section id="one">
	<div class="inner">
		<header class="major">
			<h1>Les sujets de montée en compétence</h1>
		</header>

<!-- Content -->
<h2 id="content">De quoi s'agit-il ?</h2>
<p>💡 Les sujets de montée en compétence sont des sujets verticaux ou transverses qui ne circonscrivent pas d’offre commerciale à proprement parler.<br>
Toutefois, ce sont des technologies qu’on rencontre très fréquemment sur les projets et sur lesquelles nos consultants doivent être globalement aguerris.<br>
Un effort de formation continue est donc requis.</p>
<hr class="major" />

<div class="row">
	<div class="3u 12u$(medium)">
		<h3>Développement</h3>
		<p>Kotlin
		<br>Java
		<br>.Net C#
		<br>JavaScript
		<br>React, Angular, Vue.js
		<br>TypeScript
		<br>Swift
		</p>
	</div>
	<div class="3u 12u$(medium)">
		<h3>Software factory</h3>
        <p>git
        <br>Jenkins
        <br>Github
        <br>Gitlab
        <br>Azure DevOps
        <br>SonarQube
		</p>
	</div>
	<div class="3u 12u$(medium)">
		<h3>Sécurité</h3>
		<p>DevSecOps
		<br>Culture
		<br>DevOps / plateforme
		<br>Outils de tests automatisés
		</p>
	</div>
	<div class="3u 12u$(medium)">
		<h3>Pratiques et méthodologies</h3>
		<p>Agilité
		<br>Crafts
		<br>Architecture
		<br>Design Patterns
		</p>
	</div>
</div>